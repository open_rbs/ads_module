package app.com.adsmodule;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private AdsModule adsModule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adsModule = new AdsModule(this);

        test();
    }

    public void test(){
        if(AdsTests.test(adsModule)){
            Log.d("MAIN_ACTIVITY", "WELL DONE");
        }else{
            Log.d("MAIN_ACTIVITY", "SOME ERRORS");
        }
    }
}
