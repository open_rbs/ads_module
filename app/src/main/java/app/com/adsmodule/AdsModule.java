package app.com.adsmodule;

import android.app.Activity;


/**
 * <p>Класс - модуль для работы с рекламой</p>
 * <p>С его помощью инициализируеться и регулируеться показ рекламы</p>
 * <p>Примечание - методы должны использовать друг друга по возможности для уменьшения кол-ва кода</p>
 */
public class AdsModule{

    /**
     * <p>Уникальное имя модуля</p>
     */
    public static final String NAME = "ADS_MODULE";

    /**
     * <p>События генерируемые модулем</p>
     */
    public static final String ON_AD_LOADED = "on_ad_loaded";
    public static final String ON_AD_CLOSED = "on_ad_closed";

    /**
     * <p>Уникальноый идентификатор приложения адмоб</p>
     */
    public static final String APP_ID = "application_admob_id";

    private Activity activity;

    /**
     * <p>Приватный конструктор для реализации синглтона</p>
     */
    public AdsModule(Activity _activity){
        activity = _activity;
    }

    /**
     * Инициализирует междустраничные обьявления и регистрирует слушателей событий
     * @param unit Идентификатор рекламной записи адмоб
     */
    public void init(final String unit){
        //TO DO
    }

    /**
     * Начинает загрузка межстраничного обьявления если оно еще не готово
     */
    public void prepareInterstitial(){
        //TO DO
    }

    /**
     * Показывает обьявление если оно готово к показу
     */
    public void showInterstitial(){
        //TO DO
    }
}
